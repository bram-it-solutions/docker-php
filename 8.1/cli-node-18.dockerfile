FROM mlocati/php-extension-installer AS php-extension-installer

FROM php:8.1-cli

COPY --from=php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

ARG NODE_VERSION=18

RUN apt-get -y update && \
    apt-get -y install gnupg gosu curl ca-certificates zip unzip git && \
    # install nodejs
    curl -sL "https://deb.nodesource.com/setup_${NODE_VERSION}.x" | bash - && \
    apt-get -y install nodejs && \
    npm install -g npm && \
    # install php and extensions
    install-php-extensions @composer intl zip mbstring curl dom pcov pdo pdo_mysql && \
    apt-get clean autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
RUN echo "memory_limit = 512M" >> /usr/local/etc/php/conf.d/memory_limit.ini

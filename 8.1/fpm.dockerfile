FROM mlocati/php-extension-installer AS php-extension-installer

FROM php:8.1-fpm

COPY --from=php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN apt-get -y update && \
    apt-get -y install git unzip && \
    install-php-extensions @composer intl zip mbstring curl dom pcov pdo pdo_mysql && \
    apt-get clean autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
RUN echo "memory_limit = 512M" >> /usr/local/etc/php/conf.d/memory_limit.ini
